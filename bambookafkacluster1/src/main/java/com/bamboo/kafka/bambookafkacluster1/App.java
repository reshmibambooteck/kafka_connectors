package com.bamboo.kafka.bambookafkacluster1;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.kafka.clients.producer.*;

/**
 * Hello world!
 *
 */
public class App 
{
	
	
    public static void main( String[] args )
    {
    	try 
    	{
    	Properties props = new Properties();
    	props.put("bootstrap.servers", "192.168.1.12:9092");
    	//props.put("transactional.id", "my-transactional-id");
    	props.put("acks", "all");
    	props.put("retries", 0);
    	props.put("batch.size", 16384);
    	props.put("linger.ms", 1);
    	props.put("buffer.memory", 33554432);
    	props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    	props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    	
    	   Producer<String, String> producer = new KafkaProducer
    		        <String, String>(props);
    		     
    		     System.out.println("About to send messages");

        System.out.println("HERE 1");
     
        	producer.send(new ProducerRecord("bambooserver1-cluster1", "JAVA BASED MESSAGE" ));
            System.out.println("HERE 2");
            producer.close();
       
    	}
    	catch (Exception e) {
    		 System.out.println("ISSUE IN writeData " + e);
   	 }
    	
    	
    }
    
    
}
