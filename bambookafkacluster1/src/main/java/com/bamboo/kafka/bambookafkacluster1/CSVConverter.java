package com.bamboo.kafka.bambookafkacluster1;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

//import util.properties packages
import java.util.Properties;

//import simple producer packages
import org.apache.kafka.clients.producer.Producer;

//import KafkaProducer packages
import org.apache.kafka.clients.producer.KafkaProducer;

//import ProducerRecord packages
import org.apache.kafka.clients.producer.ProducerRecord;

public class CSVConverter {

private static ArrayList<String[]> allData = new ArrayList<String[]>();
private static String[] headers;
public static void main(String[] args) {
File inputfolder = new File("C:\\Bamboo\\StepKafka\\");
File[] allfiles = inputfolder.listFiles();
for (File file : allfiles) {
getData(file);
}
writeData();
}

private static void writeData() {
try {
     // create instance for properties to access producer configs   
     Properties props = new Properties();
     
     //Assign localhost id
     props.put("bootstrap.servers", "13.82.213.23:9092");
     
     //Set acknowledgements for producer requests.      
     props.put("acks", "all");
     
     //If the request fails, the producer can automatically retry,
     props.put("retries", 0);
     
     //Specify buffer size in config
     props.put("batch.size", 16384);
     
     //Reduce the no of requests less than 0   
     props.put("linger.ms", 1);
     
     //The buffer.memory controls the total amount of memory available to the producer for buffering.   
     props.put("buffer.memory", 33554432);
     
      
     props.put("key.serializer", 
            "org.apache.kafka.common.serialization.StringSerializer");

     props.put("value.serializer", 
            "org.apache.kafka.common.serialization.StringSerializer");
     
     Producer<String, String> producer = new KafkaProducer
        <String, String>(props);
     
     System.out.println("About to send messages");
     
     for(String[] thisData : allData) {
         for (int i=0; i < headers.length; i++) {
         producer.send(new ProducerRecord<String, String>("bambootek1", Integer.toString(i), headers[i] + ":" + thisData[i]));  
         System.out.println("SENT: " + headers[i] + ":" + thisData[i]);
         }
     }

     System.out.println("Message sent successfully");
     producer.close();
 } catch (Exception e) {
 System.out.println("ISSUE IN writeData " + e);
 }
}

private static void getData(File file) {
HashMap thisData = new HashMap();
try {
 BufferedReader br1 = new BufferedReader(new FileReader(file)); 
 String st1 = br1.readLine();
 headers = st1.split(";");
 
 while ((st1 = br1.readLine()) != null) {
 allData.add(st1.split(";"));
 }
 } catch (Exception e) {
 System.out.println("ISSUE IN getData " + e);
 }
}
}
